const multiplicationTableInRows = (multiply, from, to) => {

  const validateNumber = (n) => typeof n === 'number'
  const validatePositiveNumber = (n) => n > 0
  const validateLoop = (from, to) => from <= to


  if (!validateNumber(multiply) || !validateNumber(from) || !validateNumber(to)) throw new Error('The operation cannot be performed because one or more parameters is not a number')
  if (!validatePositiveNumber(from) || !validatePositiveNumber(to)) throw new Error('The "from" and "to" parameters must to be positive')
  if (!validateLoop(from, to)) throw new Error('The "from" parameter must to be smaller of equal than "to" parameter')

  let multiplicationTable = ''
  for (let i = from; i <= to; i++) {
    let result = multiply * i
    multiplicationTable += `${multiply} * ${i} = ${result}\n`
  }
  return multiplicationTable
}



module.exports = multiplicationTableInRows